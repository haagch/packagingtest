#!/usr/bin/env python3

import gitlab
import os
import shutil

if not 'CI_JOB_TOKEN' in os.environ and not 'GITLAB_TOKEN' in os.environ:
  print("Set CI_JOB_TOKEN or GITLAB_TOKEN env var to your token")
  exit(1)

# job token authentication (to be used in CI)
# bear in mind the limitations of the API endpoints it supports:
# https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html
if 'CI_JOB_TOKEN' in os.environ:
  gl = gitlab.Gitlab('https://gitlab.freedesktop.org', job_token=os.environ['CI_JOB_TOKEN'])
elif 'GITLAB_TOKEN' in os.environ:
  gl = gitlab.Gitlab('https://gitlab.freedesktop.org', private_token=os.environ['GITLAB_TOKEN'])


# make an API request to create the gl.user object. This is not required but may be useful
# to validate your token authentication. Note that this will not work with job tokens.
# gl.auth()

# Enable "debug" mode. This can be useful when trying to determine what
# information is being sent back and forth to the GitLab server.
# Note: this will cause credentials and other potentially sensitive
# information to be printed to the terminal.

# gl.enable_debug()

project_names = [ 'gulkan', 'gxr', 'g3k', 'xrdesktop', 'libinputsynth', 'kwin-effect-xrdesktop', 'kdeplasma-applets-xrdesktop' ] # 'gnome_shell', 'gnome-shell-extension-xrdesktop']

for project_name in project_names:
  print("Processing project " + project_name + "...")
  project = gl.projects.get('xrdesktop' + '/' + project_name)
  for branch in project.branches.list(get_all=True):
    branch_name = branch.get_id()

    if branch_name != "main" and branch_name != "legacy" and not branch_name.startswith("release-"):
      # print("Ignoring branch " + branch_name)
      continue

    # TODO
    if branch_name != "main":
      continue

    print("Fetching artifacts for branch " + branch_name + "...")

    pipelines = project.pipelines.list(get_all=True)
    pipeline = pipelines[0]
    jobs = pipeline.jobs.list(get_all=True)
    #for job in jobs:
    #  job.pprint()


    print("Extracting artifacts...")

    zipfn = os.path.join(branch_name + "_" + project_name + "_" + "artifact.zip")
    with open(zipfn, "wb") as f:
      artifacts_stream = project.artifacts.download(ref_name=branch_name, job='reprepro:package', streamed=True, action=f.write)
    shutil.unpack_archive(zipfn, os.path.join("repos", branch_name, project_name))

    os.unlink(zipfn)
